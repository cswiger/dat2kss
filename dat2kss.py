#!/usr/bin/env python

import sys

if ( len(sys.argv) != 3 ):
  print("Usage: dat2kss.py <observation number> <output file>")
  sys.exit(1)


from BeautifulSoup import BeautifulSoup
import requests
url = "https://network.satnogs.org/observations/" + sys.argv[1] + "/"
page = requests.get(url)
soup = BeautifulSoup(page.content)
mydivs = soup.findAll("div", {"class": "well well-sm data-well hex"})

fh = open(sys.argv[2],"w")
for div in mydivs:
  hxdata = div.renderContents().replace('\n','').replace(' ','')
  kss = 'C000' + hxdata + 'C0'
  fh.write(kss.decode('hex'))
fh.close()


